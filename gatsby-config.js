module.exports = {
  siteMetadata: {
    siteTitle: `Powderbooking documentation`,
    defaultTitle: `Powderbooking documentation`,
    siteTitleShort: `Powderbooking Docs`,
    siteDescription: `Documentation for the Powderbooking application`,
    siteUrl: `https://readme.powderbooking.com`,
    siteAuthor: `Michael Kemna (@mrasap)`,
    siteImage: `/banner.png`,
    siteLanguage: `en`,
    themeColor: `#576fe6`,
    basePath: `/`,
  },
  plugins: [
    {
      resolve: `@rocketseat/gatsby-theme-docs`,
      options: {
        configPath: `src/config`,
        docsPath: `src/docs`,
        repositoryUrl: `https://gitlab.com/powderbooking/README`,
        baseDir: `examples/gatsby-theme-docs`,
      },
    },
    {
      resolve: `gatsby-plugin-manifest`,
      options: {
        name: `Powderbooking Documentation`,
        short_name: `Powderbooking docs`,
        start_url: `/`,
        background_color: `#ffffff`,
        display: `standalone`,
        icon: `static/favicon.png`,
      },
    },
    `gatsby-plugin-sitemap`,
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'YOUR_GOOGLE_ANALYTICS_TRACKING_ID',
      },
    },
    `gatsby-plugin-remove-trailing-slashes`,
    {
      resolve: `gatsby-plugin-canonical-urls`,
      options: {
        siteUrl: `https://readme.powderbooking.com`,
      },
    },
    `gatsby-plugin-offline`,
  ],
};
