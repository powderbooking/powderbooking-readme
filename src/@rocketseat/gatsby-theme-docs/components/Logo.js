import React from 'react';

/**
 * @description   Main Logo Component
 */
let Logo = () => {
    return (
        <h1>
            Powderbooking
        </h1>
    );
}

export default Logo;