# README
Documentation for the Powderbooking application.

The live version of the documentation can be found [here](https://readme.powderbooking.com).


## Getting started

You can help develop this repo through Docker or Gatsby. In both instances, the hot-reloading development site will be available on http://localhost:8000

### Docker
Make sure you have [Docker](https://docker.io) installed.
```bash
cd compose
docker-compose build readme
docker-compose up readme
```

### Gatsby
Make sure you have [Gatsby](https://www.gatsbyjs.com/) installed.

```bash
npm install
gatsby develop
```

## Drawio

The UML diagrams are created using [drawio](https://app.diagrams.net/). The `.drawio` files are converted to `.png` in a layer of the Dockerfile. No `.png` files are stored on the repo according to best practices.