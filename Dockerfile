# https://github.com/rlespinasse/drawio-export
FROM rlespinasse/drawio-export:4.0.0 as drawio

COPY /drawio .
# Note, this is the ENTRYPOINT of the Dockerfile
# See also: https://github.com/rlespinasse/docker-drawio-desktop-headless/blob/v1.x/Dockerfile#L39
RUN /opt/drawio-desktop/entrypoint.sh --format png --border 5 --transparent --remove-page-suffix

# ---------------------------------------------------
# ---------------------------------------------------
FROM node:buster as builder

WORKDIR /app

COPY package*.json ./
RUN npm install -g gatsby-cli && \
    npm ci

COPY . .
COPY --from=drawio /data/export/ /app/static

RUN npm run build

# ---------------------------------------------------
# ---------------------------------------------------
# loosely based on: https://medium.com/@tiangolo/react-in-docker-with-nginx-built-with-multi-stage-docker-builds-including-testing-8cc49d6ec305
FROM nginx:alpine as production

COPY --from=builder /app/public/ /usr/share/nginx/html
COPY ./nginx.conf /etc/nginx/conf.d/default.conf

# should match the port in nginx.conf
EXPOSE 80

CMD ["nginx", "-g", "daemon off;"]
